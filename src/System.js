/* @flow */

'use strict';

type Config<TState, TMeasurement, TControl, TControlState> = {
  measure: (s: TState) => TMeasurement;
  control: (c: ?TControlState, r: TMeasurement, y: TMeasurement) => [
    TControl,
    TControlState,
  ];
  process: (s: TState, u: TControl) => TState;
  initialState: TState;
  initialControlState?: ?TControlState;
};

class System<TState, TMeasurement, TControl, TControlState> {
  config: Config<TState, TMeasurement, TControl, TControlState>;
  controlState: ?TControlState;
  state: TState;

  constructor(config: Config<TState, TMeasurement, TControl, TControlState>) {
    this.config = config;
    this.controlState = config.initialControlState;
    this.state = config.initialState;
  }

  step(setPoint: TMeasurement): {
    initialState: TState,
    measurement: TMeasurement,
    controlValue: TControl,
    newState: TState,
  } {
    const {
      config: {
        process,
        control,
        measure,
      },
      state: initialState,
      controlState: initialControlState,
    } = this;
    const measurement = measure(initialState);
    const [controlValue, newControlState] = control(
      initialControlState,
      setPoint,
      measurement,
    );
    const newState = process(initialState, controlValue);
    this.state = newState;
    this.controlState = newControlState;
    return {initialState, measurement, controlValue, newState};
  }

  reset() {
    this.state = this.config.initialState;
    this.controlState = this.config.initialControlState;
  }
}

module.exports = System;
