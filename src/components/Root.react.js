/* @flow */

'use strict';

const React = require('react');
const NumberInput = require('./NumberInput.react');
const Plot = require('./Plot.react');
const System = require('../System');

function runSimulation(system, setPoint, numSteps, dt): Array<*> {
  const steps = [system.state];
  for (let i = 0; i < numSteps / dt; i++) {
    steps.push(system.step(setPoint).newState);
  }
  return steps;
}

type Props = {|
  initialDT: number,
  initialDTerm: number,
  initialPTerm: number,
  initialSetPoint: number,
  initialState: [number, number], // pos, vel
  numSteps: number,
|};

type State = {|
  dt: number,
  dTerm: number,
  pTerm: number,
  setPoint: number,
|};

class Root extends React.PureComponent<Props, State> {
  state = {
    dt: this.props.initialDT,
    dTerm: this.props.initialDTerm,
    pTerm: this.props.initialPTerm,
    setPoint: this.props.initialSetPoint,
  };

  _setDT = (dt: number) => this.setState({dt});
  _setDTerm = (dTerm: number) => this.setState({dTerm});
  _setPTerm = (pTerm: number) => this.setState({pTerm});
  _setSetPoint = (setPoint: number) => this.setState({setPoint});

  render() {
    const {initialState, numSteps} = this.props;
    const {dt, dTerm, pTerm, setPoint} = this.state;

    const system = new System({
      measure: ([pos]) => pos,
      control: (prevError, setPoint, measurement) => {
        const error = setPoint - measurement;
        let controlValue = error * pTerm;
        if (prevError != null) {
          controlValue -= (prevError - error) * dTerm;
        }
        return [controlValue, error];
      },
      process: ([pos, vel], controlValue) => ([
        pos + vel * dt,
        vel + controlValue * dt,
      ]),
      initialState,
    });

    const data = runSimulation(system, setPoint, numSteps, dt)
      .map(([pos], i) => ({x: i * dt + 1, y: pos}));

    return (
      <div>
        <NumberInput
          label="Set point"
          step={.1}
          value={setPoint}
          onChange={this._setSetPoint}
        />
        <NumberInput
          label="P"
          min={0}
          step={.001}
          value={pTerm}
          onChange={this._setPTerm}
        />
        <NumberInput
          label="D"
          min={0}
          step={.01}
          value={dTerm}
          onChange={this._setDTerm}
        />
        <NumberInput
          label="dt"
          min={.01}
          step={.01}
          value={dt}
          onChange={this._setDT}
        />
        <Plot data={data} height={300} width={710} />
      </div>
    );
  }
}

module.exports = Root;
