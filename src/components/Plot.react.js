/* @flow */

'use strict';

const React = require('react');
const {
  HorizontalGridLines,
  LineSeries,
  XAxis,
  XYPlot,
  YAxis,
} = require('react-vis');

function Plot({
  data,
  height,
  width,
}: {|
  data: Array<{
    x: number,
    y: number,
  }>,
  height: number,
  width: number,
|}) {
  return (
    <XYPlot width={710} height={300}>
      <HorizontalGridLines />
      <LineSeries data={data}/>
      <XAxis />
      <YAxis />
    </XYPlot>
  );
}

module.exports = Plot;
