/* @flow */

'use strict';

const React = require('React');
const Root = require('../Root.react');
const renderer = require('react-test-renderer');

test('Root renders', () => {
  const component = renderer.create(
    <Root
      initialDT={1}
      initialDTerm={0.1}
      initialPTerm={0.01}
      initialSetPoint={3}
      initialState={[0, 0]}
      numSteps={10}
    />,
  );

  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
