/* @flow */

'use strict';

const React = require('react');

class NumberInput extends React.PureComponent<{|
  label: string,
  min?: number,
  step: number,
  value: number,
  onChange: (value: number) => mixed,
|}> {
  _onChange = (event: SyntheticInputEvent<HTMLInputElement>) =>
    this.props.onChange(parseFloat(event.target.value));

  render() {
    const {label, min, step, value} = this.props;
    return (
      <label style={{marginRight: 10}}>
        {label}:
        <input
          min={min}
          step={step}
          style={{marginLeft: 10}}
          type="number"
          value={value}
          onChange={this._onChange}
        />
      </label>
    );
  }
}

module.exports = NumberInput;
