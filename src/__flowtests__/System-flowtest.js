/* @flow */

'use strict';

const System = require('../System');

type State = {
  pos: number,
  vel: number,
  accel: number,
};
type Measurement = number; // position
type Control = number; // acceleration
type ControlState = typeof undefined | null;

class MySystem extends System<State, Measurement, Control, ControlState> {}

new MySystem({
  measure: (state: State): Measurement => state.vel,
  control: (
    controlState: ControlState,
    setPoint: Measurement,
    measurement: Measurement,
  ): [Control, ControlState] => ([
    setPoint - measurement,
    controlState,
  ]),
  process: (state: State): State => ({...state}),
  initialState: {pos: 0, vel: 0, accel: 0},
});

new MySystem({
  measure: state => state.vel,
  control: (controlState, setPoint, measurement) => ([
    setPoint - measurement,
    controlState,
  ]),
  process: state => ({...state}),
  initialState: {pos: 0, vel: 0, accel: 0},
});

new System({
  measure: (state: State): Measurement => state.vel,
  control: (
    controlState: ControlState,
    setPoint: Measurement,
    measurement: Measurement,
  ): [Control, ControlState] => ([
    setPoint - measurement,
    controlState,
  ]),
  process: (state: State): State => ({...state}),
  initialState: {pos: 0, vel: 0, accel: 0},
});

new System({
  measure: state => state.vel,
  control: (controlState, setPoint, measurement) => ([
    setPoint - measurement,
    controlState,
  ]),
  process: state => ({...state}),
  initialState: {pos: 0, vel: 0, accel: 0},
});
