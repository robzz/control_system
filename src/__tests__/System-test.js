/* @flow */

'use strict';

const System = require('../System');

test('System.step', () => {
  const dt = 1;
  const dTerm = 0.1;
  const pTerm = 0.01;
  const setPoint = 3;
  const initialState = [0, 0];
  const numSteps = 4;

  const system = new System({
    measure: ([pos]) => pos,
    control: (prevError, setPoint, measurement) => {
      const error = setPoint - measurement;
      let controlValue = error * pTerm;
      if (prevError != null) {
        controlValue -= (prevError - error) * dTerm;
      }
      return [controlValue, error];
    },
    process: ([pos, vel], controlValue) => ([
      pos + vel * dt,
      vel + controlValue * dt,
    ]),
    initialState,
  });

  const steps = new Array(numSteps).fill(0).map(() => system.step(setPoint));
  expect(steps).toMatchSnapshot();
});
