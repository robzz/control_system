/* @flow */

'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const Root = require('./components/Root.react');

const component = (
  <Root
    initialDT={1}
    initialDTerm={0.1}
    initialPTerm={0.01}
    initialSetPoint={3}
    initialState={[0, 0]}
    numSteps={100}
  />
);

if (global.document) {
  ReactDOM.render(component, document.getElementById('root'));
} else {
  console.log('document isn\'t available, so we can\'t render anything'); // eslint-disable-line no-console
}
