# Technologies
yarn, flow, React, jest, eslint, webpack

# Installing
1. Install dependencies: `yarn install`. `yarn` is a package manager for Javascript (https://yarnpkg.com/).
2. If you want to use flow, install interface definitions for dependencies: `yarn run flow-typed-install`. `flow-typed` is a central repository for flow definitions (https://github.com/flowtype/flow-typed).

# Running stuff
There's probably three main ways to execute the code in this web app:

1. Run tests: `yarn run jest`

2a. Transpile the client JS: `yarn run build`
2b. Execute it locally: `node lib/index.js`
2c. Or do both: `yarn run build-node`

3a. Package the website: `yarn run build-web`
3b. Open `dist/index.html` in a browser

# Notes about scripts:
- Any `watch-*` scripts (see `package.json`) will continuously rerun `*` whenever the file changes. This is done with the `watchit.sh` script, which depends on watchman being installed (https://facebook.github.io/watchman/).
- Any `flow-*` scripts (see `package.json`) will run `*` only if `flow` succeeds. `flow` is a static type checker for Javascript (https://flow.org/). If you just want to run flow: `yarn run flow`.

# Notes about other stuff:
- `webpack` is used for packaging, and `babel` is used for transpiling. But I don't know how to easily reuse `babel` presets between standalone transcompilation (step 2a) and running webpack (step 3a), so please just remember to keep them in sync (see how flow and react are listed twice, in `webpack.config.js` and `.babelrc`)
