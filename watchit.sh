watchman shutdown-server
watchman watch $1

if [[ $# -ne 2 ]] ; then
  echo 'wrong number of args ("watchit src flow", for example)'
  exit 0
fi

clear
eval $2

while true; do
  watchman-wait . -p "$1/**/*.js"
  if [[ $? -eq 0 ]]; then
    echo '/////////////////////////////////////////////////////////////////////'
    clear
    eval $2
  else
    exit
  fi
  sleep 1
done
